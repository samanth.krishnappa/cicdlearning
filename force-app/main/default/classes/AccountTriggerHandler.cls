/**
 * @description Contains all the account trigger logic
 * @version 1.0 01/10/2022
 * @author  samanth.krishnappa@ceptes.com
 */
public with sharing class AccountTriggerHandler {
    
    /**
     * @description handleBeforeUpdate Handle all the operations that need to be handled before the update DML operation.
     * @param  List<Account>        newAccounts     A List of all new account changes.
     * @param  Map<Id, Account>     oldAccountMap   A Map of old version of accounts with Id as key.
     * @version 1.0 01/10/2022
     * @author  samanth.krishnappa@ceptes.com
     */
    public void handleBeforeUpdate(List<Account> newAccounts, Map<Id, Account> oldAccountMap) {
        validateAddressChange(newAccounts, oldAccountMap);
    }

    /**
     * @description validateAddressChange Validates whether the address change falls within the stipulated limit.
     * @param  List<Account>        newAccounts     A List of all new account changes.
     * @param  Map<Id, Account>     oldAccountMap   A Map of old version of accounts with Id as key.
     * @version 1.0 01/10/2022
     * @author  samanth.krishnappa@ceptes.com
     */
    private static void validateAddressChange(List<Account> newAccounts, Map<Id, Account> oldAccountMap) {
        for(Account acc : newAccounts) {
            Account oldAccount = oldAccountMap.get(acc.Id);
            String oldAddressString = getAddressAsAString(oldAccount);
            String newAddressString = getAddressAsAString(acc);
            if(!oldAddressString.equals(newAddressString)) {
                if(oldAccount.No_of_address_edits__c >= 5) {
                    acc.addError('Users are not allowed to make address edits more than 5 times, Please contact relevant Admin to get this resolved.');   
                }else {
                    acc.No_of_address_edits__c += 1;
                }
            }
        }
    }

    /**
     * @description getAddressAsAString convert the account's shipping address field into a standardized string for easy comparison
     * @param  Account  addressToBeConverted    Account who's shipping address needs to stringified.
     * @return String   stringified shipping address.
     * @version 1.0 01/10/2022
     * @author  samanth.krishnappa@ceptes.com
     */
    private static string getAddressAsAString(Account addressToBeConverted) {
        return '' + addressToBeConverted.ShippingStreet + '+$+' + addressToBeConverted.ShippingCity +
        '+||+' + addressToBeConverted.ShippingState + '+%+' + addressToBeConverted.ShippingCountry + 
        '+#+' + addressToBeConverted.ShippingPostalCode;
    }
}