@isTest
public class AccountTriggerHandlerTest {
    testMethod static void testAddressChangeValidation() {
        Account acc1 = new Account(
        	Name = 'Test Account 1',
          	ShippingStreet = '5th avenue, bakers street',
            ShippingCity = 'Chennai',
            ShippingState = 'Tamil Nadu',
            ShippingCountry = 'India',
            ShippingPostalCode = '570098',
            No_of_address_edits__c = 5
        );
        
        INSERT acc1;
        
        Account acc2 = new Account(
        	Name = 'Test Account 2',
          	ShippingStreet = '5th avenue, bakers street',
            ShippingCity = 'Chennai',
            ShippingState = 'Tamil Nadu',
            ShippingCountry = 'India',
            ShippingPostalCode = '570098',
            No_of_address_edits__c = 3
        );
        
        INSERT acc2;
        
        Test.startTest();
            try {
                acc1.ShippingCity = 'waynad';
                UPDATE acc1;
            }catch (DMLException e) {
                System.assertEquals('Users are not allowed to make address edits more than 5 times. Please contact relevant Admin to get this resolved.', e.getDmlMessage(0), 'Error Message should be as expected');
            }
            
            acc2.ShippingCity = 'waynad';
            UPDATE acc2;
        Test.stopTest();
        
        Set<Id> accountIds = new Set<Id> { acc1.Id, acc2.Id };
        Map<Id, Account> updatedAccountsMap = new Map<Id, Account>([SELECT Id,No_of_address_edits__c FROM Account WHERE Id IN:accountIds ]);
        
        System.assertEquals(5, updatedAccountsMap.get(acc1.Id).No_of_address_edits__c, 'Does not get updated beyond 5 times');
        System.assertEquals(4, updatedAccountsMap.get(acc2.Id).No_of_address_edits__c, 'Since, less than 5 times, gets incremented by 1');
    }
}